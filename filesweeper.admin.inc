<?php

/**
 * @file
 * Administration forms for File sweeper.
 *
 * @see filesweeper.module
 */

/**
 * File sweeper main page callback.
 */
function filesweeper_page() {

  $output = array();

  $wrappers = file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE);

  foreach (array_keys($wrappers) as $wrapper) {
    $mask = '/.*/';
    $options['nomask'] = variable_get('filesweeper_nomask_pattern', FILESWEEPER_NOMASK_DEFAULT);
    $directory = file_scan_directory($wrapper . "://", $mask, $options);

    $unmanaged = array();
    foreach ($directory as $uri => $file) {
      if (!filesweeper_is_managed($uri)) {
        if (is_writable($uri)) {
          $actions = array(
            l(t('add'), 'admin/config/media/filesweeper/add', array('query' => array('file' => $uri))),
            l(t('delete'), 'admin/config/media/filesweeper/delete', array('query' => array('file' => $uri))),
          );
        }
        else {
          $actions = array(t('not writable'));
        }

        if (filesize($uri) > 1048576) {
          $filesize = array('@size' => round(filesize($uri) / 1048576), '@units' => 'MB');
        }
        elseif (filesize($uri) > 1024) {
          $filesize = array('@size' => round(filesize($uri) / 1024), '@units' => 'KB');
        }
        else {
          $filesize = array('@size' => filesize($uri), '@units' => 'bytes');
        }

        $unmanaged[$uri] = array(
          l($uri, file_create_url($uri)),
          file_get_mimetype($uri),
          t('@size @units', $filesize),
          format_date(filemtime($uri), 'short'),
          implode(' ', $actions),
        );
      }
    }

    $output[] = array(
      '#prefix' => '<h3>',
      '#markup' => $wrapper,
      '#suffix' => '</h3>',
    );

    if (!empty($unmanaged)) {
      $output[] = array(
        '#theme' => 'table',
        '#header' => array(t('URI'), t('Type'), t('Size'), t('Updated'), t('Actions')),
        '#rows' => $unmanaged,
      );
    }
    else {
      $output[] = array(
        '#prefix' => '<div>',
        '#markup' => t('No unmanaged files found for %wrapper.', array('%wrapper' => $wrapper)),
        '#suffix' => '</div>',
      );
    }
  }

  return $output;

}

/**
 * Determine if a file is managed by the system.
 */
function filesweeper_is_managed($uri) {

  $result = db_select('file_managed', 'f')
    ->fields('f', array('fid'))
    ->condition('f.uri', $uri, '=')
    ->execute()
    ->fetchAll();
  return !empty($result);

}

/**
 * Add file to system.
 *
 * @see file_save_upload()
 */
function filesweeper_add_file() {
  $query = drupal_get_query_parameters();
  if (empty($query['file'])) {
    drupal_set_message(t('No file specified to add. Please try again.'), 'warning');
    drupal_goto('admin/config/media/filesweeper');
  }
  $uri = $query['file'];
  if (!is_file($uri)) {
    drupal_set_message(t('File @uri not found. Please try again.', array('@uri' => $uri)), 'warning');
    drupal_goto('admin/config/media/filesweeper');
  }
  if (!is_writable($uri)) {
    drupal_set_message(t('File @uri not writable. Please try again.', array('@uri' => $uri)), 'warning');
    drupal_goto('admin/config/media/filesweeper');
  }

  global $user;

  // Begin building file object.
  $file = new stdClass();
  $file->uid = $user->uid;
  $file->status = 1;
  $file->filename = trim(drupal_basename($uri), '.');
  $file->uri = $uri;
  $file->filemime = file_get_mimetype($uri);
  $file->filesize = filesize($uri);

  if (!$file = file_save($file)) {
    drupal_set_message(t('Error adding file %uri', array('%uri' => $uri)), 'error');
  }
  drupal_goto('admin/config/media/filesweeper');
}

/**
 * Deletion confirmation form.
 */
function filesweeper_delete_form($form, &$form_state) {
  $query = drupal_get_query_parameters();
  if (empty($query['file'])) {
    drupal_set_message(t('No file specified for deletion. Please try again.'), 'warning');
    drupal_goto('admin/config/media/filesweeper');
  }
  $uri = $query['file'];
  if (!is_file($uri)) {
    drupal_set_message(t('File @uri not found. Please try again.', array('@uri' => $uri)), 'warning');
    drupal_goto('admin/config/media/filesweeper');
  }
  if (!is_writable($uri)) {
    drupal_set_message(t('File @uri not writable. Please try again.', array('@uri' => $uri)), 'warning');
    drupal_goto('admin/config/media/filesweeper');
  }

  $form = array();

  $form ['uri'] = array('#type' => 'value', '#value' => $uri);

  return confirm_form($form,
   t('Are you sure you want to delete %uri?', array('%uri' => $uri)),
   'admin/config/media/filesweeper',
   t('This action cannot be undone.'),
   t('Delete'),
   t('Cancel')
  );
}

/**
 * Executes file deletion.
 */
function filesweeper_delete_form_submit($form, &$form_state) {
  if ($form_state ['values']['confirm']) {
    if (!file_unmanaged_delete($form_state['values']['uri'])) {
      drupal_set_message(t('Error deleting file %uri', array('%uri' => $uri)), 'error');
    }
  }
  $form_state ['redirect'] = 'admin/config/media/filesweeper';
}

/**
 * Settings page.
 */
function filesweeper_settings_form($form, &$form_state) {
  $form['filesweeper_nomask_pattern'] = array(
    '#title' => t('Exclusions mask'),
    '#description' => t('Enter a regular expression of files to exclude from the listing.'),
    '#type' => 'textfield',
    '#size' => 128,
    '#maxlength' => 1024,
    '#default_value' => variable_get('filesweeper_nomask_pattern', FILESWEEPER_NOMASK_DEFAULT),
    '#required' => TRUE,
  );

  return system_settings_form($form);

}
